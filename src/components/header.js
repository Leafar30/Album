// import libraries for making components
import React from 'react';
import { Text, View } from 'react-native';
// Make a component
const Header = (props) => {
    const { textStyle, viewStyle } = styles;

return (
   
    <View style={viewStyle}>
        <Text style={textStyle}>{props.headerText}</Text>
    </View>
);
};

const styles = {
 viewStyle: {
     backgroundColor: 'grey',
     justifyContent: 'center',
     alignItems: 'center',
     height: 60,
     padding: 15,
     borderWidth: 2,
     borderRadius: 10,
     borderColor: '#ddd',
     elevation: 2,
     position: 'relative'    
 },

 textStyle: {
     fontSize: 20
 }
};
// Make the component available to other parts of the app
export default Header;